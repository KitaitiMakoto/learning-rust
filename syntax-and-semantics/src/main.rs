fn main() {
    loop46();
}

fn loop46() {
    for (i, j) in (5..10).enumerate() {
        println!("i = {} and j = {}", i, j);
    }

    let lines = "hello\nworld".lines();
    for (linenumber, line) in lines.enumerate() {
        println!("{}: {}", linenumber, line);
    }

    let mut x = 5;
    loop {
        x += x - 3;

        println!("{}", x);

        if x % 5 == 0 { break; }
    }

    for x in 0..10 {
        if x % 2 == 0 { continue; }

        println!("{}", x);
    }

    'outer: for x in 0..10 {
        'inner: for y in 0..10 {
            if x % 2 == 0 { continue 'outer; }
            if y % 2 == 0 { continue 'inner; }
            println!("x: {}, y: {}", x, y);
        }
    }
}
