require "fiddle/import"

module Hello
  extend Fiddle::Importer
  dlload "target/release/libembed.dylib"
  extern "void process()"
end

Hello.process

puts "done!"
