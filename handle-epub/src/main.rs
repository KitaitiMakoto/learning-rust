extern crate zip;

use std::fs;
use std::io::Read;

fn main() {
    let path = std::env::args().nth(1).unwrap();
    let file = fs::File::open(&path).unwrap();

    let mut archive = zip::ZipArchive::new(file).unwrap();

    for i in 0..archive.len() {
        let mut f = archive.by_index(i).unwrap();
        let name = f.name().to_string();
        println!("{}", name);
    }

    let f = archive.by_name("META-INF/container.xml").unwrap();
    println!("{:?}", f.bytes().next().unwrap());
}
