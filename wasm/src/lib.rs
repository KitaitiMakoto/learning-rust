extern {
    fn log(ptn: *const u8, number: i32);
}

#[no_mangle]
pub fn sum(a: i32, b:i32) -> i32 {
    unsafe {
        log(a as *const u8, a);
        log(b as *const u8, b);
    }
    a + b
}
